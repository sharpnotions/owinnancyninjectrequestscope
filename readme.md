SN.OwinNancyNinjectRequestScope
---

This library provides a request scope for Ninject bindings within Nancy which does not rely upon System.Web or upon child kernels.

To utilize this capability:

1. Include a reference to this library.
2. Call the `.UseRequestScopeMiddleware()` in your Owin application startup before you include Nancy in the pipeline.
3. Inherit your Nancy bootstrapper from the `NancyRequestScopedNinjectBootstrapper` included in the `SN.OwinNancyNinjectRequestScope` namespace.
4. Reference the `Ninject.SNOwinRequestScope` namespace in your Ninject modules.
5. Utilize the `.InOwinRequestScope()` extension to specify request scope lifetime for any bindings that may need it.

---

This library utilizes modified code from the Nancy mainline repository. The modification provides access to the Owin context during module binding, allowing the scope object to be supplied as a binding parameter during the Nancy bootstrapper's module activation.

The original use of ChildKernel within the Nancy bootstrapper is not modified. The `.InOwinRequestScope()` extension method provides request scoping from within the bootstrapper's "application" level kernel.

---

This code and library is provided under the MIT license. See LICENSE.txt for details.