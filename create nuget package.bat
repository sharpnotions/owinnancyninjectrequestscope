@echo off
set /p VersionNumber= Enter version number: 
.\.nuget\nuget.exe pack src\SN.OwinRequestScope\SN.OwinRequestScope.nuspec -Version %VersionNumber%
.\.nuget\nuget.exe pack src\SN.OwinNinjectRequestScope\SN.OwinNancyNinjectRequestScope.nuspec -Version %VersionNumber%