﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ninject;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Diagnostics;
using Nancy.Owin;
using SN.OwinRequestScope.Nancy.Infrastructure;
using Ninject.Extensions.OwinRequestScope;

namespace SN.OwinRequestScope.Nancy
{

    /// <summary>
    /// Nancy bootstrapper for the Ninject container.
    /// </summary>
    /// <remarks>
    /// This bootstrapper properly implements per-request scoping for Ninject containers along
    /// the lines of what 
    /// </remarks>
    public abstract class NancyRequestScopedNinjectBootstrapper : NancyBootstrapperBase<IKernel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NancyRequestScopedNinjectBootstrapper"/> class.
        /// </summary>
        protected NancyRequestScopedNinjectBootstrapper() : this(new StandardKernel())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NancyRequestScopedNinjectBootstrapper"/> class
        /// with the specified externally defined container.
        /// </summary>
        /// <param name="externalKernel">The external kernel.</param>
        protected NancyRequestScopedNinjectBootstrapper(IKernel externalKernel)
        {
            //IMPORTANT: make sure to register this bootstrap as the one and only true bootstrapper
            NancyBootstrapperLocator.Bootstrapper = this;

            //back to the actually important stuff, like 
            ExternalKernelInstance = externalKernel;
            externalKernel.Load(new FactoryModule());
        }

        /// <summary>
        /// Context key for storing the child container in the context
        /// </summary>
        private const string CONTEXT_KEY = "NancyRequestScopedNinjectBootstrapper";

        /// <summary>
        /// Gets the context key for storing the child container in the context
        /// </summary>
        protected virtual string ContextKey
        {
            get { return CONTEXT_KEY; }
        }

        /// <summary>
        /// True if the request startup tasks have been registered
        /// </summary>
        private bool RequestStartupTasksRegistered = false;

        /// <summary>
        /// The request startup task registration lock
        /// </summary>
        private static object RequestStartupTaskRegistrationLock = new object();

        /// <summary>
        /// Gets or sets the application container instance.
        /// </summary>
        /// <value>
        /// The application container instance.
        /// </value>
        private IKernel ExternalKernelInstance { get; set; }

        /// <summary>
        /// Create a default, unconfigured, container
        /// </summary>
        /// <returns>Container instance</returns>
        protected override IKernel GetApplicationContainer()
        {
            return ExternalKernelInstance;
        }

        /// <summary>
        /// Gets the diagnostics for intialisation
        /// </summary>
        /// <returns>An <see cref="IDiagnostics"/> implementation</returns>
        protected override IDiagnostics GetDiagnostics()
        {
            return this.ApplicationContainer.Get<IDiagnostics>();
        }

        /// <summary>
        /// Gets all registered application startup tasks
        /// </summary>
        /// <returns>An <see cref="System.Collections.Generic.IEnumerable{T}"/> instance containing <see cref="IApplicationStartup"/> instances. </returns>
        protected override IEnumerable<IApplicationStartup> GetApplicationStartupTasks()
        {
            return this.ApplicationContainer.GetAll<IApplicationStartup>();
        }

        /// <summary>
        /// Registers the request startup tasks.
        /// </summary>
        /// <remarks>
        /// This is called before any attempt to retrieve the configured Nancy engine
        /// </remarks>
        private void RegisterRequestStartupTasks()
        {
            if (!RequestStartupTasksRegistered)
            {
                var registrationTasks = RequestStartupTasks;
                if (registrationTasks != null && registrationTasks.Any())
                {
                    //lock should be needed only during initial startup to prevent multiple threads
                    //from registering startup tasks simultaneously
                    lock (RequestStartupTaskRegistrationLock)
                    {
                        if (!RequestStartupTasksRegistered)
                        {
                            foreach (var task in registrationTasks)
                            {
                                ApplicationContainer.Bind<IRequestStartup>().To(task).InOwinRequestScope();
                            }
                        }
                        RequestStartupTasksRegistered = true;
                    }
                }
                RequestStartupTasksRegistered = true;
            }
        }

        /// <summary>
        /// Gets all registered request startup tasks
        /// </summary>
        /// <returns>An <see cref="IEnumerable{T}"/> instance containing <see cref="IRequestStartup"/> instances.</returns>
        [Obsolete("This method does not properly provide request context.")]
        protected override sealed IEnumerable<IRequestStartup> RegisterAndGetRequestStartupTasks(IKernel container, Type[] requestStartupTypes)
        {
            return container.GetAll<IRequestStartup>();
        }

        /// <summary>
        /// Gets all registered application registration tasks
        /// </summary>
        /// <returns>An <see cref="System.Collections.Generic.IEnumerable{T}"/> instance containing <see cref="IRegistrations"/> instances.</returns>
        protected override IEnumerable<IRegistrations> GetRegistrationTasks()
        {
            return this.ApplicationContainer.GetAll<IRegistrations>();
        }

        /// <summary>
        /// Get INancyEngine
        /// </summary>
        /// <returns>An <see cref="INancyEngine"/> implementation</returns>
        protected override sealed INancyEngine GetEngineInternal()
        {
            RegisterRequestStartupTasks();

            return this.ApplicationContainer.Get<INancyEngine>();
        }

        /// <summary>
        /// Get all <see cref="INancyModule"/> implementation instances
        /// </summary>
        /// <param name="context">The current context</param>
        /// <returns>An <see cref="IEnumerable{T}"/> instance containing <see cref="INancyModule"/> instances.</returns>
        public override sealed IEnumerable<INancyModule> GetAllModules(NancyContext context)
        {
            var owinContext = context.GetOwinEnvironment();

            //var parameter = owinContext == null ? new OwinContextNinjectParameter() : new OwinContextNinjectParameter(owinContext);

            return ApplicationContainer.GetAll<INancyModule>(context.GetRequestScopeParameter(true));
        }

        /// <summary>
        /// Retrieves a specific <see cref="INancyModule"/> implementation - should be per-request lifetime
        /// </summary>
        /// <param name="moduleType">Module type</param>
        /// <param name="context">The current context</param>
        /// <returns>The <see cref="INancyModule"/> instance</returns>
        public override sealed INancyModule GetModule(Type moduleType, NancyContext context)
        {
            return ApplicationContainer.Get(moduleType, context.GetRequestScopeParameter()) as INancyModule;
        }

        /// <summary>
        /// Resolves a request for a service within the current request's scope
        /// </summary>
        /// <typeparam name="T">The type of service to resolve</typeparam>
        /// <param name="context">The <see cref="NancyContext"/> for the request.</param>
        /// <returns>The service</returns>
        /// <exception cref="System.ArgumentNullException">If the context argument is null</exception>
        public T GetRequestScopedService<T>(NancyContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return ApplicationContainer.Get<T>(context.GetRequestScopeParameter());
        }

        /// <summary>
        /// Returns a <see cref="Ninject.Parameters.IParameter"/> you can use to access request-scoped ninject bindings.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Ninject.Parameters.IParameter GetRequestScopeParameter(NancyContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return context.GetRequestScopeParameter();
        }

        /// <summary>
        /// Creates and initializes the request pipelines.
        /// </summary>
        /// <param name="context">The <see cref="NancyContext"/> used by the request.</param>
        /// <returns>An <see cref="IPipelines"/> instance.</returns>
        protected override sealed IPipelines InitializeRequestPipelines(NancyContext context)
        {
            var requestPipelines =
                new Pipelines(this.ApplicationPipelines);

            if (this.RequestStartupTaskTypeCache.Any())
            {
                var startupTasks = ApplicationContainer.GetAll<IRequestStartup>(context.GetRequestScopeParameter());

                foreach (var requestStartup in startupTasks)
                {
                    requestStartup.Initialize(requestPipelines, context);
                }
            }
            
            this.RequestStartup(ApplicationContainer, requestPipelines, context);

            return requestPipelines;
        }

        /// <summary>
        /// Takes the registration tasks and calls the relevant methods to register them
        /// </summary>
        /// <param name="registrationTasks">Registration tasks</param>
        protected override sealed void RegisterRegistrationTasks(IEnumerable<IRegistrations> registrationTasks)
        {
            foreach (var applicationRegistrationTask in registrationTasks)
            {
                //bring in all of the type registrations
                var applicationTypeRegistrations = applicationRegistrationTask.TypeRegistrations == null ?
                                                        new TypeRegistration[] { } :
                                                        applicationRegistrationTask.TypeRegistrations.ToArray();

                //bring in all of the collection registrations
                var applicationCollectionRegistrations = applicationRegistrationTask.CollectionTypeRegistrations == null ?
                                                            new CollectionTypeRegistration[] { } :
                                                            applicationRegistrationTask.CollectionTypeRegistrations.ToArray();

                //get all of the instance registrations
                var applicationInstanceRegistrations = applicationRegistrationTask.InstanceRegistrations;

                this.RegisterTypes(this.ApplicationContainer, applicationTypeRegistrations);
                this.RegisterCollectionTypes(this.ApplicationContainer, applicationCollectionRegistrations);

                if (applicationInstanceRegistrations != null)
                {
                    this.RegisterInstances(this.ApplicationContainer, applicationInstanceRegistrations);
                }
            }
        }

        /// <summary>
        /// Bind the bootstrapper's implemented types into the container.
        /// This is necessary so a user can pass in a populated container but not have
        /// to take the responsibility of registering things like <see cref="INancyModuleCatalog"/> manually.
        /// </summary>
        /// <param name="applicationContainer">Application container to register into</param>
        protected override sealed void RegisterBootstrapperTypes(IKernel applicationContainer)
        {
            applicationContainer.Bind<INancyModuleCatalog>().ToConstant(this);
        }

        /// <summary>
        /// Register the given module types into the container
        /// </summary>
        /// <param name="container">Container to register into</param>
        /// <param name="moduleRegistrationTypes">NancyModule types</param>
        protected override sealed void RegisterModules(IKernel container, IEnumerable<ModuleRegistration> moduleRegistrationTypes)
        {
            foreach (var reg in moduleRegistrationTypes)
            {
                //modules should exist within request scope
                container.Bind<INancyModule>().To(reg.ModuleType).InTransientScope();
            }
        }

        /// <summary>
        /// Bind the given instances into the container
        /// </summary>
        /// <param name="container">Container to register into</param>
        /// <param name="instanceRegistrations">Instance registration types</param>
        protected override void RegisterInstances(IKernel container, IEnumerable<InstanceRegistration> instanceRegistrations)
        {
            foreach (var instanceRegistration in instanceRegistrations)
            {
                container.Bind(instanceRegistration.RegistrationType).ToConstant(instanceRegistration.Implementation);
            }
        }

        /// <summary>
        /// Bind the default implementations of internally used types into the container as singletons
        /// </summary>
        /// <param name="container">Container to register into</param>
        /// <param name="typeRegistrations">Type registrations to register</param>
        protected override sealed void RegisterTypes(IKernel container, IEnumerable<TypeRegistration> typeRegistrations)
        {
            foreach (var typeRegistration in typeRegistrations)
            {
                switch (typeRegistration.Lifetime)
                {
                    case Lifetime.Transient:
                        container.Bind(typeRegistration.RegistrationType).To(typeRegistration.ImplementationType).InTransientScope();
                        break;
                    case Lifetime.Singleton:
                        container.Bind(typeRegistration.RegistrationType).To(typeRegistration.ImplementationType).InSingletonScope();
                        break;
                    case Lifetime.PerRequest:
                        container.Bind(typeRegistration.RegistrationType).To(typeRegistration.ImplementationType).InOwinRequestScope();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        
        /// <summary>
        /// Bind the various collections into the container as singletons to later be resolved
        /// by IEnumerable{Type} constructor dependencies.
        /// </summary>
        /// <param name="container">Container to register into</param>
        /// <param name="collectionTypeRegistrations">Collection type registrations to register</param>
        protected override sealed void RegisterCollectionTypes(IKernel container, IEnumerable<CollectionTypeRegistration> collectionTypeRegistrations)
        {
            foreach (var collectionTypeRegistration in collectionTypeRegistrations)
            {
                foreach (var implementationType in collectionTypeRegistration.ImplementationTypes)
                {
                    switch (collectionTypeRegistration.Lifetime)
                    {
                        case Lifetime.Transient:
                            container.Bind(collectionTypeRegistration.RegistrationType).To(implementationType).InTransientScope();
                            break;
                        case Lifetime.Singleton:
                            container.Bind(collectionTypeRegistration.RegistrationType).To(implementationType).InSingletonScope();
                            break;
                        case Lifetime.PerRequest:
                            container.Bind(collectionTypeRegistration.RegistrationType).To(implementationType).InOwinRequestScope();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Extension method class to simplify getting the ninject scope parameter
    /// </summary>
    internal static class NancyContextScopeExtension
    {
        /// <summary>
        /// Gets the request scope parameter.
        /// </summary>
        /// <param name="ctx">The nancy context.</param>
        /// <param name="defaultToSingleton">if set to <c>true</c> the scope will resolve to singleton scope when the OWIN environment isn't available, e.g. during startup.</param>
        /// <returns>The <see cref="OwinScopeParameter"/></returns>
        /// <exception cref="System.InvalidOperationException">Cannot use request scope, because there is no OWIN Environment on the Nancy context and <paramref name="defaultToSingleton"/> is false.</exception>
        public static OwinScopeParameter GetRequestScopeParameter(this NancyContext ctx, bool defaultToSingleton = false)
        {
            var env = ctx.GetOwinEnvironment();
            if (env == null)
            {
                //owin environment isn't available during Nancy startup
                if (defaultToSingleton)
                    return OwinScopeParameter.CreateSingletonScopeParameter();
                throw new InvalidOperationException("Cannot use request scope, because there is no OWIN Environment on the Nancy context.");
            }
            else
            {
                return (OwinScopeParameter)env[RequestScopeSettings.ENVIRONMENT_KEY];
            }
        }
    }
}