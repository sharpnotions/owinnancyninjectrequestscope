﻿using System;
using Ninject.Infrastructure.Disposal;

namespace SN.OwinRequestScope.Infrastructure
{
    /// <summary>
    /// A scoping object which implements Ninject's disposal infrastructure interface.
    /// </summary>
    internal class OwinScope : INotifyWhenDisposed
    {
        /// <summary>
        /// The hash of this fully qualified class name
        /// </summary>
        private static readonly int CLASS_NAME_HASH = typeof(OwinScope).FullName.GetHashCode();
        
        /// <summary>
        /// Initializes a new instance of the <see cref="OwinScope"/> class.
        /// </summary>
        public OwinScope()
        {
            Disposed += (s, e) => { };
            RequestScopeId = Guid.NewGuid();
        }

        /// <summary>
        /// Gets a value indicating whether is disposed.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// An arbitrary and unique identifier for the request scope
        /// </summary>
        public Guid RequestScopeId { get; private set; }

        /// <summary>
        /// The disposed event.
        /// </summary>
        public event EventHandler Disposed;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                Disposed(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            //performing an XOR on the private GUID to ensure that the hash for this object is distinguishable
            //from the hash of the GUID itself
            return (RequestScopeId.GetHashCode() ^ CLASS_NAME_HASH);
        }

        /// <summary>
        /// Determines whether the specified object, is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified object is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            //simply ensure the object is of this type, then compare the hash codes
            return obj is OwinScope && obj.GetHashCode() == this.GetHashCode();
        }
    }
}
