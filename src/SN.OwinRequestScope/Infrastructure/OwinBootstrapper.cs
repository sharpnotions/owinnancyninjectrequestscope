﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Ninject.Extensions.OwinRequestScope;

namespace SN.OwinRequestScope.Infrastructure
{
    /// <summary>
    /// The OwinBootstrapper supplies a scope object consumed by any request-lifetime bindings 
    /// registered in Ninject. The scope object is disposed of following the request's
    /// completion, causing Ninject to dispose of any objects that were activated during that
    /// timeframe.
    /// </summary>
    internal class OwinBootstrapper
    {

        /// <summary>
        /// Execute this step of the OWIN pipeline
        /// </summary>
        /// <param name="context">The OWIN context, to which this request is attached.</param>
        /// <param name="next">The next task in the pipeline.</param>
        /// <returns>An asynchronous task to be executed by the pipeline.</returns>
        public async Task Execute(IOwinContext context, Func<Task> next)
        {
            using (var scope = new OwinScope())
            {
                context.Set(RequestScopeSettings.ENVIRONMENT_KEY, new OwinScopeParameter(scope));
                await next();
                context.Set<OwinScope>(RequestScopeSettings.ENVIRONMENT_KEY, null);
            }
        }
    }
}
