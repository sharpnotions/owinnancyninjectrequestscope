﻿using SN.OwinRequestScope.Infrastructure;

namespace Owin
{
    /// <summary>
    /// Provides an extension for the OWIN <see cref="IAppBuilder"/> interface which will enable per-request scoping.
    /// </summary>
    /// <remarks>
    /// This middleware must be included before Nancy if you are to take advantage of request scoping in Ninject.
    /// </remarks>
    public static class RequestScopeAppBuilderExtensions
    {
        /// <summary>
        /// Provides request scope support to applications running via the OWIN pipeline.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The application.</returns>
        public static IAppBuilder UseRequestScopeMiddleware(this IAppBuilder app)
        {
            return app.Use(new OwinBootstrapper().Execute);
        }
    }
}
