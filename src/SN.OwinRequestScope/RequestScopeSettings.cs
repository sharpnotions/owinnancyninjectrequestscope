﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.OwinRequestScope
{
    /// <summary>
    /// Various settings which may need to be accessed by external libraries
    /// </summary>
    public static class RequestScopeSettings
    {
        /// <summary>
        /// The key for the request scope within the OWIN environment dictionary.
        /// </summary>
        public const string ENVIRONMENT_KEY = "sn.requestscope";
    }
}
