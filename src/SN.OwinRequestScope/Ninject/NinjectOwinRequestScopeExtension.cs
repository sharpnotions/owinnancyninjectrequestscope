﻿using System.Linq;
using Ninject.Activation;
using Ninject.Syntax;

namespace Ninject.Extensions.OwinRequestScope
{
    /// <summary>
    /// Defines an extension method for Ninject bindings, allowing the user to scope their
    /// binding depending on the lifetime of the request.
    /// </summary>
    public static class NinjectOwinRequestScopeExtension
    {
        /// <summary>
        /// Specifies a binding as having request-level lifetime scoping.
        /// </summary>
        /// <typeparam name="T">The type of the binding.</typeparam>
        /// <param name="syntax">The binding syntax tree being scoped.</param>
        /// <returns>The scoped binding syntax tree.</returns>
        public static IBindingNamedWithOrOnSyntax<T> InOwinRequestScope<T>(this IBindingInSyntax<T> syntax)
        {
            return syntax.InScope(GetScope);
        }

        /// <summary>
        /// Gets the scope object by examining the <see cref="IContext"/> activation request.
        /// </summary>
        /// <remarks>
        /// If at any point the system does not work properly, this method returns null.
        /// In which case, the objects are scoped as transient object.
        /// </remarks>
        /// <param name="arg">The activation context for this request.</param>
        /// <returns>A scoping object, or null if the system is not working.</returns>
        private static object GetScope(IContext arg)
        {
            var param = arg.Request.Parameters
                .OfType<OwinScopeParameter>()
                .FirstOrDefault();

            if (param != null)
            {
                Planning.Targets.ITarget target = null;
                if (arg.Request != null)
                {
                    target = arg.Request.Target;
                }

                return param.GetValue(arg, target);
            }
            return null;
        }
    }
}
