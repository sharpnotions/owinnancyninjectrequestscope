﻿using System;
using Ninject.Activation;
using Ninject.Parameters;
using Ninject.Planning.Targets;
using SN.OwinRequestScope.Infrastructure;

namespace Ninject.Extensions.OwinRequestScope
{
    /// <summary>
    /// A ninject resolution parameter that provides request scoping capabilities. If the
    /// current request cannot be determined, objects will resolve in transient scope.
    /// </summary>
    public class OwinScopeParameter : IParameter
    {
        /// <summary>
        /// The ninject parameter name, aka the full type name, but it's defined by the interface.
        /// </summary>
        public static readonly string NINJECT_PARAMETER_NAME = typeof(OwinScopeParameter).FullName;

        /// <summary>
        /// Initializes a new instance of the <see cref="OwinScopeParameter"/> class with the default scope object.
        /// </summary>
        private OwinScopeParameter() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwinScopeParameter"/> class. If null is specified for the
        /// <paramref name="scope"/> then any activated objects will use the singleton scope.
        /// </summary>
        /// <param name="scope">The scope.</param>
        internal OwinScopeParameter(OwinScope scope)
        {
            if (scope == null) throw new ArgumentNullException("");
            _scope = scope;
        }

        /// <summary>
        /// The scope object used for all request-scoped bindings resolved during this resolution request.
        /// </summary>
        private OwinScope _scope = null;

        /// <summary>
        /// Gets the name of the parameter.
        /// </summary>
        public string Name
        {
            get { return NINJECT_PARAMETER_NAME; }
        }

        /// <summary>
        /// Gets a value indicating whether the parameter should be inherited into child requests.
        /// </summary>
        public bool ShouldInherit
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the value for the parameter within the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="target">The target.</param>
        /// <returns>
        /// The value for the parameter.
        /// </returns>
        public object GetValue(IContext context, ITarget target)
        {
            if (_scope != null)
            {
                return _scope;
            }
            else
            {
                var targetName = "unknown target";
                if (target != null && target.Type != null)
                {
                    targetName = target.Type.FullName;
                }
                System.Diagnostics.Trace.TraceWarning("Resolving service [{0}] for target [{1}] in Singleton scope, rather than OwinRequestScope", context.Binding.Service.FullName, targetName);
                return Ninject.Infrastructure.StandardScopeCallbacks.Singleton(context);
            }
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(IParameter other)
        {
            return other.Name == this.Name && other.GetHashCode() == this.GetHashCode();
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return (_scope == null ? 0 : _scope.GetHashCode()) ^ NINJECT_PARAMETER_NAME.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj is OwinScopeParameter && obj.GetHashCode() == this.GetHashCode();
        }

        /// <summary>
        /// Creates a unique scope parameter, not bound to singleton or any OWIN context
        /// </summary>
        /// <returns>A <see cref="OwinScopeParameter"/> parameter configured for a unique scope.</returns>
        public static IDisposable CreateStaticScopeParameter(out OwinScopeParameter outParam)
        {
            var scope = new OwinScope();
            outParam = new OwinScopeParameter(scope);
            return scope;
        }

        /// <summary>
        /// Creates the a scope parameter compatible with OWIN scope, but bound to the singleton scope.
        /// </summary>
        /// <returns>A <see cref="OwinScopeParameter"/> configured for singleton scope.</returns>
        public static OwinScopeParameter CreateSingletonScopeParameter()
        {
            return new OwinScopeParameter();
        }
    }
}
